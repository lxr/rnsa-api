<?php
$time_start = microtime(true);
require 'flight/Flight.php';
require 'flight/include.php';

Flight::set('timestart',microtime(true));
Flight::set('flight.views.path', './flight/vues');
Flight::set('log_errors', false);
Flight::set('version', 'alpha-0.0.1');

Flight::set('nomApp', 'Pollens Arka');
Flight::set('url', 'https://pollens.arka.gq');

Flight::route('GET /', function(){

    Flight::render('home',Array(), 'body');

    global $time_start;
	Flight::render('base', Array('start'=>$time_start, 'titre'=>'Home'));

});


Flight::route('/departement/@d:[0-9][0-9]', function($d){

	echo json_encode(getInfoDep($d));

});

Flight::route('/departement/all', function(){

	$res = array();
	$dep;
	for($i = 1; $i < 96; $i++)
	{
		if($i < 10)
		{
			$dep = sprintf("%02d", $i);
		}
		else if($i == 20)
		{
			continue;
		}
		else
			$dep = $i;

		$res[] = getInfoDep($dep);
	}

	echo json_encode($res);

});


Flight::start();


?>
