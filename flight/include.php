<?php

function initBase() // initialisation + première population
{
	unlink(dirname(__FILE__).'/db.sqlite');

	try
	{
		$pdo = new PDO('sqlite:'.dirname(__FILE__).'/db.sqlite');
		$pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); // ERRMODE_WARNING | ERRMODE_EXCEPTION | ERRMODE_SILENT
	} 
	catch(Exception $e) 
	{
	    echo "Impossible d'accéder à la base de données SQLite : ".$e->getMessage();
	    die();
	}

	$pdo->query("CREATE TABLE IF NOT EXISTS pollens ( 
    id            INTEGER         PRIMARY KEY AUTOINCREMENT ,
    idPollen      INTEGER,
    nom           VARCHAR( 250 ),
    val           INTEGER,
    department    INTEGER
	);");

	foreach(depIterator() as $dep)
	{
		$infos = getInfoDepFromWeb($dep);

		$idp = 0;
		foreach ($infos["risks"] as $r) 
		{
			$stmt = $pdo->prepare("INSERT INTO pollens (idPollen, nom, val, department) VALUES(:idp, :nom, :val, :department)");
			$r = $stmt->execute(array(
				'idp'           => $idp,
			    'val'           => 0, // pour différencier la maj et l'init
			    'nom'           => $r["name"],
				'department'    => $dep
			));

			$idp++;
		}
	}
}

function majBase() // initialisation + première population
{
	try
	{
		$pdo = new PDO('sqlite:'.dirname(__FILE__).'/db.sqlite');
		$pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); // ERRMODE_WARNING | ERRMODE_EXCEPTION | ERRMODE_SILENT
	} 
	catch(Exception $e) 
	{
	    echo "Impossible d'accéder à la base de données SQLite : ".$e->getMessage();
	    die();
	}

	foreach(depIterator() as $dep)
	{
		$infos = getInfoDepFromWeb($dep);

		$idp = 0;
		foreach ($infos["risks"] as $r) 
		{
			// maj
		    $stmt = $pdo->prepare("UPDATE pollens SET val = :val WHERE idPollen = :idp AND department=:dep");
			$r = $stmt->execute(array(
				'idp'            => $idp,
				'dep'           => $dep,
			    'val'           => $r['risk']
			));

			$idp++;
		}
	}
}

function getInfoDep($id)
{
	try
	{
		$pdo = new PDO('sqlite:'.dirname(__FILE__).'/db.sqlite');
		$pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); // ERRMODE_WARNING | ERRMODE_EXCEPTION | ERRMODE_SILENT
	} 
	catch(Exception $e) 
	{
	    echo "Impossible d'accéder à la base de données SQLite : ".$e->getMessage();
	    die();
	}

	$dep = $id;

	$stmt = $pdo->prepare("SELECT * FROM pollens WHERE department=:id");
	$stmt->execute(array("id"=>$id));
	$result = $stmt->fetchAll();

	$j = 1;
	$res = array();
	$res["return"] = "OK"; // a retravailler !
	$res["department"][] = array("id"=>$dep, "name"=>getDepName($dep), "number"=>$dep);
	while ($j != 19)
	{
	  $res["risks"][] = array("name"=>$result[$j]["nom"], "risk"=>$result[$j]["val"]);
	  $j++;
	}

	return $res;
}

function getInfoDepFromWeb($id)
{
	if($id > 95)
		die("Département introuvable");

	$dep = $id;

	//detail
	$i = 1;
	$im = imagecreatefromgif("http://internationalragweedsociety.org/vigilance/d%20".$dep.".gif");
	$x = 116;$y = 45;
	while ($i != 20)
	{
		$rgb = imagecolorat($im, $x, $y);
		$colors = imagecolorsforindex($im, $rgb);
		if ($colors['red'] == 255 && $colors['green'] == 255 && $colors['blue'] == 255 || $colors['red'] == 135 && $colors['green'] == 135 && $colors['blue'] == 135)
		{
		   $pollens[$i]["color"] = "black";
		   $pollens[$i]["class"] = 0;
		}
		elseif ($colors['red'] == 0 && $colors['green'] == 255 && $colors['blue'] == 0)
		{
		   $pollens[$i]["color"] = "green";
		   $pollens[$i]["class"] = 1;
		}
		elseif ($colors['red'] == 0 && $colors['green'] == 176 && $colors['blue'] == 80)
		{
		   $pollens[$i]["color"] = "yellow";
		   $pollens[$i]["class"] = 2;
		}
		elseif ($colors['red'] == 255 && $colors['green'] == 255 && $colors['blue'] == 0)
		{
		   $pollens[$i]["color"] = "orange";
		   $pollens[$i]["class"] = 3;
		}
		elseif ($colors['red'] == 247 && $colors['green'] == 150 && $colors['blue'] == 70)
		{
		   $pollens[$i]["color"] = "red";
		   $pollens[$i]["class"] = 4;
		}
		else
		{
		   $pollens[$i]["color"] = "violet";
		   $pollens[$i]["class"] = 5;
		}

		$y = $y + 20;
		$i++;
	}

	$pollens["goals"]["min"] = 0;
	$pollens["goals"]["max"] = 5;
	$pollens[0]["name"] = "general";
	$pollens[1]["name"] = "Cupressacées";
	$pollens[2]["name"] = "Noisetier";
	$pollens[3]["name"] = "Aulne";
	$pollens[4]["name"] = "Peuplier";
	$pollens[5]["name"] = "Saule";
	$pollens[6]["name"] = "Frêne";
	$pollens[7]["name"] = "Charme";
	$pollens[8]["name"] = "Bouleau";
	$pollens[9]["name"] = "Platane";
	$pollens[10]["name"] = "Chêne";
	$pollens[11]["name"] = "Olivier";
	$pollens[12]["name"] = "Tilleul";
	$pollens[13]["name"] = "Châtaignier";
	$pollens[14]["name"] = "Rumex";
	$pollens[15]["name"] = "Graminées";
	$pollens[16]["name"] = "Plantain";
	$pollens[17]["name"] = "Urticacées";
	$pollens[18]["name"] = "Armoises";
	$pollens[19]["name"] = "Ambroises";


	//affiche directement les donnees sur la page web
	$j = 1;
	$res = array();
	$res["return"] = "OK"; // a retravailler !
	$res["department"][] = array("id"=>$dep, "name"=>getDepName($dep), "number"=>$dep);
	while ($j != 20)
	{
	  $res["risks"][] = array("name"=>$pollens[$j]["name"], "risk"=>$pollens[$j]["class"]);
	  //echo "<font color=".$pollens[$j]["color"].">".$pollens[$j]["name"].": ".$pollens[$j]["class"]."</font><br>";
	  $j++;
	}

	return $res;
}

function getDepName($id)
{
	$departements = array(); 

	$departements['01'] = 'Ain'; 
	$departements['02'] = 'Aisne'; 
	$departements['03'] = 'Allier'; 
	$departements['04'] = 'Alpes de Haute Provence'; 
	$departements['05'] = 'Hautes Alpes'; 
	$departements['06'] = 'Alpes Maritimes'; 
	$departements['07'] = 'Ardèche'; 
	$departements['08'] = 'Ardennes'; 
	$departements['09'] = 'Ariège'; 
	$departements['10'] = 'Aube'; 
	$departements['11'] = 'Aude'; 
	$departements['12'] = 'Aveyron'; 
	$departements['13'] = 'Bouches du Rhône'; 
	$departements['14'] = 'Calvados'; 
	$departements['15'] = 'Cantal'; 
	$departements['16'] = 'Charente'; 
	$departements['17'] = 'Charente Maritime'; 
	$departements['18'] = 'Cher'; 
	$departements['19'] = 'Corrèze';
	$departements['20'] = 'Corse';  
	$departements['2A'] = 'Corse du Sud'; 
	$departements['2B'] = 'Haute Corse'; 
	$departements['21'] = 'Côte d\'Or'; 
	$departements['22'] = 'Côtes d\'Armor'; 
	$departements['23'] = 'Creuse'; 
	$departements['24'] = 'Dordogne'; 
	$departements['25'] = 'Doubs';
	$departements['26'] = 'Drôme'; 
	$departements['27'] = 'Eure'; 
	$departements['28'] = 'Eure et Loir'; 
	$departements['29'] = 'Finistère'; 
	$departements['30'] = 'Gard'; 
	$departements['31'] = 'Haute Garonne'; 
	$departements['32'] = 'Gers'; 
	$departements['33'] = 'Gironde'; 
	$departements['34'] = 'Hérault'; 
	$departements['35'] = 'Ille et Vilaine'; 
	$departements['36'] = 'Indre'; 
	$departements['37'] = 'Indre et Loire'; 
	$departements['38'] = 'Isère'; 
	$departements['39'] = 'Jura'; 
	$departements['40'] = 'Landes'; 
	$departements['41'] = 'Loir et Cher'; 
	$departements['42'] = 'Loire'; 
	$departements['43'] = 'Haute Loire'; 
	$departements['44'] = 'Loire Atlantique'; 
	$departements['45'] = 'Loiret'; 
	$departements['46'] = 'Lot'; 
	$departements['47'] = 'Lot et Garonne'; 
	$departements['48'] = 'Lozère'; 
	$departements['49'] = 'Maine et Loire'; 
	$departements['50'] = 'Manche'; 
	$departements['51'] = 'Marne'; 
	$departements['52'] = 'Haute Marne'; 
	$departements['53'] = 'Mayenne'; 
	$departements['54'] = 'Meurthe et Moselle'; 
	$departements['55'] = 'Meuse'; 
	$departements['56'] = 'Morbihan'; 
	$departements['57'] = 'Moselle'; 
	$departements['58'] = 'Nièvre'; 
	$departements['59'] = 'Nord'; 
	$departements['60'] = 'Oise'; 
	$departements['61'] = 'Orne'; 
	$departements['62'] = 'Pas de Calais'; 
	$departements['63'] = 'Puy de Dôme'; 
	$departements['64'] = 'Pyrénées Atlantiques'; 
	$departements['65'] = 'Hautes Pyrénées'; 
	$departements['66'] = 'Pyrénées Orientales'; 
	$departements['67'] = 'Bas Rhin'; 
	$departements['68'] = 'Haut Rhin'; 
	$departements['69'] = 'Rhône-Alpes'; 
	$departements['70'] = 'Haute Saône'; 
	$departements['71'] = 'Saône et Loire'; 
	$departements['72'] = 'Sarthe'; 
	$departements['73'] = 'Savoie'; 
	$departements['74'] = 'Haute Savoie'; 
	$departements['75'] = 'Paris'; 
	$departements['76'] = 'Seine Maritime'; 
	$departements['77'] = 'Seine et Marne'; 
	$departements['78'] = 'Yvelines'; 
	$departements['79'] = 'Deux Sèvres'; 
	$departements['80'] = 'Somme'; 
	$departements['81'] = 'Tarn'; 
	$departements['82'] = 'Tarn et Garonne'; 
	$departements['83'] = 'Var'; 
	$departements['84'] = 'Vaucluse'; 
	$departements['85'] = 'Vendée'; 
	$departements['86'] = 'Vienne'; 
	$departements['87'] = 'Haute Vienne'; 
	$departements['88'] = 'Vosges'; 
	$departements['89'] = 'Yonne'; 
	$departements['90'] = 'Territoire de Belfort'; 
	$departements['91'] = 'Essonne'; 
	$departements['92'] = 'Hauts de Seine'; 
	$departements['93'] = 'Seine St Denis'; 
	$departements['94'] = 'Val de Marne'; 
	$departements['95'] = 'Val d\'Oise'; 

	return $departements[$id];
}

function depIterator()
{
	// Permet de faire un foreach sur un itérateur contenant les numéros des départements français ! (hors DOM)
	$res = array();

	for($i = 1; $i < 96; $i++) // 95 premiers départements (corse = 20)
	{
		if($i < 10)
		{
			$dep = sprintf("%02d", $i); // ajoute le 0 pour les numéros < 10
		}
		else
			$dep = $i;

		$res[] = $dep;
	}
	// On ajoute la corse !

	return $res;
}

?>