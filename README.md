## Introduction ##
Ce script utilise la puissance du framework FlightPHP afin de créer une API sur les niveaux de pollen en France !


## Pré-requis ##
- PHP 5.6+
- PHP GD
- PHP sqlite


## Installation ##
- `cd /var/www`
- `git clone https://gitlab.com/lxr/rnsa-api.git`
- `chown -R www-data:www-data rnsa-api/`

-> Il faut mettre en place la tâche CRON qui est dans le dossier flight

Et c'est parti !


## TODO ##
- [x] Mise en place API v1 \
- [x] Sauvegarde en BDD avec SQlite pour limiter le nombre de requête
- [ ] AutoConf de la BDD SQlite


## Demo ##
https://pollens.arka.gq/
